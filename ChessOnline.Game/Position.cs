﻿using System;
using System.Collections.Generic;

public class Position
{
	
	private List<Move> history;
    public Board Board  { get;  } 
	public List<Figure> Figures { get;  }
	public Side ActivePlayer { get; set; }



	public Position()
	{
		Board = new Board();
		Figures = new List<Figure>() { };
		Pawn.SetYourself(this);
	}

	internal bool IsLocationOccupied(Location location)
	{
		foreach (var figure in Figures)
		{
			if (location == figure.Location)
			{
				return true;
			}
		}
		return false;
	}


	public bool ValidateMove(Move move)
	{
		return true;
	}

	public void ApplyMove(Move move)
	{
        if (!ValidateMove(move))
        {
			throw new ArgumentException($"Cannot apply invalid Move {move}");
        }


	}

	public void WriteToConsole()
    {
        for (int file = 0; file <8 ; file++)
        {
            for (int rank = 0; rank < 8; rank++)
            {
				var loc = new Location(rank, file);
                if (IsLocationOccupied(loc))
                {
                    Console.Write(GetFigureAt(loc));
                }
                else
                {
                    Console.Write(".");
                }

            }
            Console.WriteLine("");
        }

    }

	public Figure GetFigureAt(Location location)
	{
		foreach (var figure in Figures)
		{
			if (figure.Location == location)
			{
				return figure;
			}
		}
		throw new ArgumentException($"No figure at {location}") ;
	}
}

