﻿using System;
using System.Collections.Generic;

public class Board
{
	public List<Location> locations;
	public Board()
	{
		locations = new List<Location> { };
		for (int i = 0; i < 8; i++)
		{
			for (int j = 0; j < 8; j++)
			{
				var newLocation = new Location(i, j);
				locations.Add(newLocation);
			}
		}	
	}
}

public class Game
{
	private Position Position { get; set; }
}

public class Figure
{
	public Location Location { get; set; }
	public Side Side { get; set;  }
	public Piece Piece { get; set; }
	public Position Position { get; set; }

	public static void SetYourself(Position position) {  }

	public virtual List<Move> GetAvailableMoves()
	{
		throw new NotImplementedException();
	}

    public Figure()
    {

    }


}




public enum Piece
{
	none,
	pawn,
	knight,
	bishop,
	rook,
	queen,
	king
}

public enum Side
{ 
	Black,
	White
}



