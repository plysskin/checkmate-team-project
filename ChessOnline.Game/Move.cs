﻿public record Move
{
	public Piece Piece { get; set; }
	public Location From { get; set; }
	public Location To { get; set; }
	public bool IsCapture { get; set; }
	public Piece PromoteInto { get; set; }

	public Move(Piece piece, Location loc, Location locTo, bool isCapture, Piece promoteInto)
    {
		Piece = piece;
		From = loc;
		To = locTo;
		IsCapture = IsCapture;
		PromoteInto = promoteInto;
    }
}

