﻿using System;
using System.Collections.Generic;

public class Pawn:Figure
{
    public Pawn()
    {
			
    }

	public Pawn(Location _location, Side side, Position position)
	{
		Location = _location;
		Side = side;
		Position = position;
	}


	public override List<Move> GetAvailableMoves()
	{
		var _moves = new List<Move>();
		int forwardDrirection = 0;

		if (this.Side == Side.White) //проверим может ли она ходить вперед.
			forwardDrirection = 1;
		else
			forwardDrirection = -1;

        Location forwardLocation = new Location(Location.Rank, Location.File + forwardDrirection);

		if (!Position.IsLocationOccupied(forwardLocation))
        {
			_moves.Add(new Move(Piece, Location, forwardLocation, false, Piece.none));
        }

		return _moves;
	}

    internal static new void SetYourself(Position position)
    {
        for (int file = 0; file < 8; file++)
        {
			var loc1 = new Location(file, 1);
			var loc2 = new Location(file, 6);
			var wp = new Pawn(loc1,Side.White,position);
			var bp = new Pawn(loc2,Side.Black,position);
			position.Figures.Add(wp);
			position.Figures.Add(bp);
		}
	}

    public override string ToString()
    {
		if (Side == Side.Black)
			return "P";
		else
			return "p";
    }

}

