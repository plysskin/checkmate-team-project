﻿using System;
using ChessOnline.Extensions;
using ChessOnline.MessageProtocol;
using ChessOnline.MessageProtocol.Requests;
using ChessOnline.MessageProtocol.Responses;

namespace ChessOnline
{
    public class GameProcessor : IProcessor
    {
        public Response<U> OnRequest<T, U>(Request<T> request)
        {
            // var responseBody = request.MessageType switch
            // {
            //     MessageType.StartGame => (U) Convert.ChangeType(StartGame((request.Body as StartGameRequest)), typeof(U)),
            //     //Получение идентификатора и имени нового игрока, отправка на клиента
            //     MessageType.NewUser => (U)Convert.ChangeType(ApplyMove((request.Body as ApplyMoveRequest)), typeof(U)),
            //     //Получение идентификатора и имени соперника, отправка на клиента
            //     _ => throw new ChessOnlineException($"Unrecognized message type: {request.MessageType}.",
            //         ErrorType.ERR_UNEXPECTED)
            // };
            //
            // return new Response<U>(true, responseBody, null);

            return null;
        }
        
        // private UpdateBoardResponse StartGame(StartGameRequest ids)
        // {
        //     var Game = new Game(); //TODO - должно зависеть от id игроков, возвращается инициализированная доска и фигуры
        //
        //     return new UpdateBoardResponse
        //     {
        //         Figures = new System.Collections.Generic.List<FigureInfo>()
        //     };
        // }
        //
        // private ApplyMoveResponse ApplyMove(ApplyMoveRequest move)
        // {
        //     //TODO AppleMove из Game
        //
        //     return new ApplyMoveResponse
        //     {
        //         PieceID = move.PieceID,
        //         NewPositionX = move.NewPositionX,
        //         NewPositionY = move.NewPositionY,
        //         IsValid = true,
        //         ReasonToInvalidateMove = ""
        //     };
        // }
    }
}