﻿using System;
using System.Diagnostics.CodeAnalysis;



public record Location
{
	public int Rank { get; set; }
	public int File { get; set; }
	
	public Location(int rank, int file)
	{
		Rank = rank;
		File = file;
	}
	public Location(string cellString)
	{
        if (cellString.Length!=2)
        {
			throw new ArgumentOutOfRangeException($"cell too long: {cellString}");
        }
		Rank = Int32.Parse(cellString[1].ToString())-1;
		File = cellString[0] - (int)'a';
	}

	public override string ToString()
	{
		char c = (char)(Rank + (int)'a');
		string result = $"{c}{File+1}";
		return result;
	}

	//// ЭТО ОСТАЛОСЬ ОТ C# 8.0
	//public override int GetHashCode()
	//{

	//	return this.Rank * 8 + this.File;
	//}


	//public override bool Equals(object location)
	//{
	//	if (location is null)
	//	{
	//		return false;
	//	}
	//	if (!(location is Location))
	//	{
	//		return base.Equals(location);
	//	}
	//	return (Rank == (location as Location).Rank) && (File == (location as Location).File);
	//}

	//public bool Equals([AllowNull] Location other)
	//{
	//	if (other is null)
	//	{
	//		return false;
	//	}
	//	if (!(other is Location))
	//	{
	//		return base.Equals(other);
	//	}
	//	return (Rank == (other as Location).Rank) && (File == (other as Location).File);
	//}

	//public static bool operator ==(Location loc1, Location loc2)
	//{
	//	if (ReferenceEquals(loc1, loc2))
	//	{
	//		return true;
	//	}
	//	if ((loc1 is null) || (loc2 is null))
	//	{
	//		return false;
	//	}
	//	return loc1.Equals(loc2);

	//}
	//public static bool operator !=(Location loc1, Location loc2)
	//{
	//	return !(loc1 == loc2);

	//}


}

 



