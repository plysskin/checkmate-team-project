using System;

namespace ChessOnline.GameV2
{
    public record Player(Guid Id, FigureColor Color);
}