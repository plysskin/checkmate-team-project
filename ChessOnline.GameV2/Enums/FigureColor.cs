namespace ChessOnline.GameV2
{
    public enum FigureColor
    {
        Black,
        White
    }

    public static class FigureColorExtensions
    {
        public static FigureColor Inverse(this FigureColor color)
        {
            return color == FigureColor.Black ? FigureColor.White : FigureColor.Black;
        }
    }
}