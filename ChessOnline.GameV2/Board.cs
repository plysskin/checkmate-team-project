using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using ChessOnline.Extensions;

namespace ChessOnline.GameV2
{
    public class Board
    {
        private List<Figure> _figures;

        public Board()
        {
            InitFigures();
        }
        
        private void InitFigures()
        {
            _figures = new List<Figure>
            {
                new King(FigureColor.White),
                new King(FigureColor.Black),

                new Queen(FigureColor.White),
                new Queen(FigureColor.Black),

                new Rook(FigureColor.White, 1),
                new Rook(FigureColor.White, 2),
                new Rook(FigureColor.Black, 1),
                new Rook(FigureColor.Black, 2),

                new Knight(FigureColor.White, 1),
                new Knight(FigureColor.White, 2),
                new Knight(FigureColor.Black, 1),
                new Knight(FigureColor.Black, 2),

                new Bishop(FigureColor.White, 1),
                new Bishop(FigureColor.White, 2),
                new Bishop(FigureColor.Black, 1),
                new Bishop(FigureColor.Black, 2)                
            };

            for (int i = 1; i <= 8; i++)
            {
                _figures.Add(new Pawn(FigureColor.White, i));
                _figures.Add(new Pawn(FigureColor.Black, i));
            }
        }

        public Figure GetFigure(Guid id)
        {
            var figure = _figures.FirstOrDefault(f => f.Id == id);

            if (figure == null)
            {
                throw new ChessOnlineException("Не удалось найти фигуру с ID: {id}.", ErrorType.ERR_SERVER);
            }

            return figure;
        }

        public IEnumerable<Figure> GetAllFigures()
        {
            return _figures.AsEnumerable();
        }

        #region Moves Validation

        public bool IsValidMove(Figure figure, Location to)
        {
            var currentToFigure = _figures.FirstOrDefault(f => f.CurrentLocation == to);
            return currentToFigure == null || currentToFigure.Color != figure.Color && currentToFigure is not King;
        }

        #endregion
    }
}