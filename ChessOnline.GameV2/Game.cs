using System;
using System.Collections;
using System.Collections.Generic;

namespace ChessOnline.GameV2
{
    public class Game
    {
        private readonly Board _board;
        
        public Guid Id { get; }
        public Guid WhitePlayer { get; }
        public Guid BlackPlayer { get; }
        
        public Game(Guid firstPlayer, Guid secondPlayer)
        {
            _board = new();
            Id = Guid.NewGuid();

            var (whitePlayer, blackPlayer) = GeneratePlayerColors(firstPlayer, secondPlayer);
            WhitePlayer = whitePlayer;
            BlackPlayer = blackPlayer;
        }

        private static (Guid WhitePlayer, Guid BlackPlayer) GeneratePlayerColors(Guid firstPlayer, Guid secondPlayer)
        {
            return new Random().Next(0, 2) == 0 ? (firstPlayer, secondPlayer) : (secondPlayer, firstPlayer);
        }

        public bool CheckPlayers(Guid firstPlayer, Guid secondPlayer)
        {
            return WhitePlayer == firstPlayer && BlackPlayer == secondPlayer
                   || WhitePlayer == secondPlayer && BlackPlayer == firstPlayer;
        }

        public bool ApplyMove(Guid figureId, Location to)
        {
            var figure = _board.GetFigure(figureId);
            var isValid = figure.IsValidMove(to) && _board.IsValidMove(figure, to);

            if (isValid)
            {
                figure.CurrentLocation = to;
            }

            return isValid;
        }

        public IEnumerable<Figure> GetAllFigures()
        {
            return _board.GetAllFigures();
        }
    }
}