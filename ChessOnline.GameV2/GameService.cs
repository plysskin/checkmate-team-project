using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using ChessOnline.Extensions;

namespace ChessOnline.GameV2
{
    public class GameService
    {
        private readonly ConcurrentBag<Game> _games;

        public GameService()
        {
            _games = new();
        }

        public StartGameStatus StartGame(Guid firstPlayer, Guid secondPlayer, out Guid gameId
            , out Guid whitePlayer, out Guid blackPlayer)
        {
            var game = _games.FirstOrDefault(g => g.CheckPlayers(firstPlayer, secondPlayer));
            var gameExists = game != null;
            
            if (!gameExists)
            {
                game = new Game(firstPlayer, secondPlayer);
                _games.Add(game);
            }

            gameId = game.Id;
            whitePlayer = game.WhitePlayer;
            blackPlayer = game.BlackPlayer;
            
            return gameExists ? StartGameStatus.AlreadyStarted : StartGameStatus.Started;
        }

        public IEnumerable<Figure> GetAllFigures(Guid gameId)
        {
            return GetGame(gameId).GetAllFigures();
        }

        public bool ApplyMove(Guid gameId, Guid figureId, int newLocationX, int newLocationY)
        {
            return GetGame(gameId).ApplyMove(figureId, new Location(newLocationX, newLocationY));
        }

        private Game GetGame(Guid id)
        {
            var game = _games.FirstOrDefault(g => g.Id == id);
            
            if (game == null)
            {
                throw new ChessOnlineException($"Не удалось найти игру с ID {id}.", ErrorType.ERR_DATA);
            }

            return game;
        }
    }
}