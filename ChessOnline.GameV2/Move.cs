namespace ChessOnline.GameV2
{
    public class Move
    {
        public Location From { get; init; }
        public Location To { get; init; }
    }
}