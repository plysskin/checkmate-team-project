﻿using System;
using System.Linq;
using ChessOnline.Extensions;
using ChessOnline.MessageProtocol;
using ChessOnline.MessageProtocol.Requests;
using ChessOnline.MessageProtocol.Responses;

namespace ChessOnline.GameV2
{
    public class GameV2Processor : IProcessor
    {
        private readonly GameService _gameService;
        
        public GameV2Processor()
        {
            _gameService = new();
        }
        
        public Response<U> OnRequest<T, U>(Request<T> request)
        {
            var responseBody = request.MessageType switch
            {
                MessageType.StartGame => (U) Convert.ChangeType(OnStartGame(request.Body as StartGameRequest), typeof(U)),
                MessageType.ApplyMove => (U) Convert.ChangeType(OnApplyMove(request.Body as ApplyMoveRequest), typeof(U)),
                _ => throw new ChessOnlineException($"Unrecognized message type: {request.MessageType}.",
                    ErrorType.ERR_UNEXPECTED)
            };
            
            return new Response<U>(true, responseBody, null);
        }

        private StartGameResponse OnStartGame(StartGameRequest request)
        {
            var status = _gameService.StartGame(request.FirstPlayer, request.SecondPlayer, out var gameId
                , out var whitePlayer, out var blackPlayer);
            var figures = _gameService.GetAllFigures(gameId);

            return new ()
            {
                Status = status.ToString(),
                GameId = gameId,
                Players = new []
                {
                    new PlayerInfo { Color = FigureColor.White.ToString(), Id = whitePlayer },
                    new PlayerInfo { Color = FigureColor.Black.ToString(), Id = blackPlayer }
                },
                Figures = figures.Select(ConvertToInfo).ToArray()
            };
        }

        private FigureInfo ConvertToInfo(Figure figure)
        {
            return new()
            {
                Id = figure.Id,
                Color = figure.Color.ToString(),
                Type = figure.FigureType,
                LocationX = figure.CurrentLocation.X,
                LocationY = figure.CurrentLocation.Y
            };
        }

        private ApplyMoveResponse OnApplyMove(ApplyMoveRequest request)
        {
            var isValid = _gameService.ApplyMove(request.GameId, request.FigureId, request.NewLocationX
                , request.NewLocationY);

            return new ApplyMoveResponse
            {
                FigureId = request.FigureId,
                IsValid = isValid,
                NewPositionX = isValid ? request.NewLocationX : 0,
                NewPositionY = isValid ? request.NewLocationY : 0
            };
        }
    }
}