using System;

namespace ChessOnline.GameV2
{
    public class King : Figure
    {
        public override string FigureType => "King";
        
        public King(FigureColor color) : base(color) { }

        protected override Location GetInitialLocation(int number)
        {
            return Color switch
            {
                FigureColor.Black => new("E8"),
                FigureColor.White => new("E1"),
                _ => throw new ArgumentOutOfRangeException(nameof(Color))
            };
        }

        public override bool IsValidMove(Location to)
        {
            if (CurrentLocation == to)
            {
                return false;
            }
            
            var (xOffset, yOffset) = CurrentLocation.GetOffsets(to);
            return xOffset <= 1 && yOffset <= 1;
        }
    }
}