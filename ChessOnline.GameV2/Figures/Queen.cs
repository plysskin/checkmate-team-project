using System;

namespace ChessOnline.GameV2
{
    public class Queen : Figure
    {
        public override string FigureType => "Queen";
        
        public Queen(FigureColor color) : base(color) { }

        protected override Location GetInitialLocation(int _)
        {
            return Color switch
            {
                FigureColor.Black =>  new("D8"),
                FigureColor.White => new("D1"),
                _ => throw new ArgumentOutOfRangeException(nameof(Color))
            };
        }

        public override bool IsValidMove(Location to)
        {
            if (CurrentLocation == to)
            {
                return false;
            }

            var (xOffset, yOffset) = CurrentLocation.GetOffsets(to);
            return xOffset == yOffset || xOffset == 0 || yOffset == 0;
        }
    }
}