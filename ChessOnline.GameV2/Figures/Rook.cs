using System;

namespace ChessOnline.GameV2
{
    public class Rook : Figure
    {
        public override string FigureType => "Rook";
        
        public Rook(FigureColor color, int number) : base(color, number) { }

        protected override Location GetInitialLocation(int number)
        {
            return Color switch
            {
                FigureColor.Black when number == 1 =>  new("A8"),
                FigureColor.Black when number == 2 => new("H8"),
                FigureColor.White when number == 1 => new("A1"),
                FigureColor.White when number == 2 => new("H1"),
                _ => throw new ArgumentOutOfRangeException(nameof(Color))
            };
        }

        public override bool IsValidMove(Location to)
        {
            if (CurrentLocation == to)
            {
                return false;
            }

            var (xOffset, yOffset) = CurrentLocation.GetOffsets(to);
            return xOffset == 0 || yOffset == 0;
        }
    }
}