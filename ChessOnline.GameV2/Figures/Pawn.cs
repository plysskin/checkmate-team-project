using System;

namespace ChessOnline.GameV2
{
    public class Pawn : Figure
    {
        public override string FigureType => "Pawn";
        public bool NoMoveHistory { get; set; }

        public Pawn(FigureColor color, int number) : base(color, number)
        {
            NoMoveHistory = true;
        }

        protected override Location GetInitialLocation(int number)
        {
            return Color switch
            {
                FigureColor.Black when number == 1 =>  new("A7"),
                FigureColor.Black when number == 2 => new("B7"),
                FigureColor.Black when number == 3 => new("C7"),
                FigureColor.Black when number == 4 => new("D7"),
                FigureColor.Black when number == 5 => new("E7"),
                FigureColor.Black when number == 6 => new("F7"),
                FigureColor.Black when number == 7 => new("G7"),
                FigureColor.Black when number == 8 => new("H7"),
                FigureColor.White when number == 1 => new("A2"),
                FigureColor.White when number == 2 => new("B2"),
                FigureColor.White when number == 3 => new("C2"),
                FigureColor.White when number == 4 => new("D2"),
                FigureColor.White when number == 5 => new("E2"),
                FigureColor.White when number == 6 => new("F2"),
                FigureColor.White when number == 7 => new("G2"),
                FigureColor.White when number == 8 => new("H2"),
                _ => throw new ArgumentOutOfRangeException(nameof(Color))
            };
        }

        public override bool IsValidMove(Location to)
        {
            if (CurrentLocation == to || Color == FigureColor.White && to.Y < CurrentLocation.Y 
                                      || Color == FigureColor.Black && to.Y > CurrentLocation.Y)
            {
                return false;
            }

            var (xOffset, yOffset) = CurrentLocation.GetOffsets(to);

            if (xOffset > 0)
            {
                return false;
            }
            
            return yOffset == 2 && NoMoveHistory || yOffset == 1;
        }
    }
}