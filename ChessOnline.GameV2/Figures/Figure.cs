using System;

namespace ChessOnline.GameV2
{
    public abstract class Figure
    {
        public Guid Id { get; }
        public Location CurrentLocation { get; set; }
        public FigureColor Color { get; }
        
        public abstract string FigureType { get; }

        protected Figure(FigureColor color, int number = 1)
        {
            Color = color;
            CurrentLocation = GetInitialLocation(number);
            Id = Guid.NewGuid();
        }

        public abstract bool IsValidMove(Location to);

        protected abstract Location GetInitialLocation(int number);
    }
}