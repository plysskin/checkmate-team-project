using System;

namespace ChessOnline.GameV2
{
    public class Bishop : Figure
    {
        public override string FigureType => "Bishop";
        
        public Bishop(FigureColor color, int number) : base(color, number) { }

        protected override Location GetInitialLocation(int number)
        {
            return Color switch
            {
                FigureColor.Black when number == 1 =>  new("C8"),
                FigureColor.Black when number == 2 => new("F8"),
                FigureColor.White when number == 1 => new("C1"),
                FigureColor.White when number == 2 => new("F1"),
                _ => throw new ArgumentOutOfRangeException(nameof(Color))
            };
        }

        public override bool IsValidMove(Location to)
        {
            if (CurrentLocation == to)
            {
                return false;
            }

            var (xOffset, yOffset) = CurrentLocation.GetOffsets(to);
            return xOffset == yOffset;
        }
    }
}