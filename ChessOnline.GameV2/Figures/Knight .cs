using System;

namespace ChessOnline.GameV2
{
    public class Knight : Figure
    {
        public override string FigureType => "Knight";
        
        public Knight(FigureColor color, int number) : base(color, number) { }

        protected override Location GetInitialLocation(int number)
        {
            return Color switch
            {
                FigureColor.Black when number == 1 =>  new("B8"),
                FigureColor.Black when number == 2 => new("G8"),
                FigureColor.White when number == 1 => new("B1"),
                FigureColor.White when number == 2 => new("G1"),
                _ => throw new ArgumentOutOfRangeException(nameof(Color))
            };
        }

        public override bool IsValidMove(Location to)
        {
            if (CurrentLocation == to)
            {
                return false;
            }

            var (xOffset, yOffset) = CurrentLocation.GetOffsets(to);
            return xOffset == 2 && yOffset == 1 || xOffset == 1 && yOffset == 2;
        }
    }
}