using System;
using System.Collections.Generic;
using ChessOnline.Extensions;
using ChessOnline.MessageProtocol.Requests;

namespace ChessOnline.GameV2
{
    public class Location : IEquatable<Location>
    {
        private const int MinCoordinate = 1;
        private const int MaxCoordinate = 8;
            
        public int X { get; private set; }
        public int Y { get; private set; }
        
        public Location(int x, int y)
        {
            CheckCoordinates(x, y);
            X = x;
            Y = y;
        }

        public Location(string locationStr)
        {
            ParseFromString(locationStr);
        }
        
        public (int XOffset, int YOffset) GetOffsets(Location another)
        {
            return (Math.Abs(another.X - X), Math.Abs(another.Y - Y));
        }

        private void CheckCoordinates(int x, int y)
        {
            CheckCoordinate(x, "X");
            CheckCoordinate(y, "Y");
        }

        private void CheckCoordinate(int coordinate, string coordinateName)
        {
            if (coordinate is < MinCoordinate or > MaxCoordinate)
            {
                throw new ChessOnlineException($"Неверная координата расположения {coordinateName}: {coordinate}."
                    , ErrorType.ERR_SERVER);
            }
        }

        private void ParseFromString(string location)
        {
            const int locationLength = 2;

            if (string.IsNullOrWhiteSpace(location) || location.Length != locationLength)
            {
                throw new ArgumentNullException(nameof(location));
            }

            X = char.ToLower(location[0]) switch
            {
                'a' => 1,
                'b' => 2,
                'c' => 3,
                'd' => 4,
                'e' => 5,
                'f' => 6,
                'g' => 7,
                'h' => 8,
                _ => throw new ArgumentException($"Неверная строка расположения фигуры: {location}.")
            };
            
            if (!char.IsDigit(location[1]) || (Y = (int) char.GetNumericValue(location[1])) < MinCoordinate
                                          || Y > MaxCoordinate)
            {
                throw new ArgumentException($"Неверная строка расположения фигуры: {location}.");
            }
        }

        #region Equality Methods

        public bool Equals(Location other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return X == other.X && Y == other.Y;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Location) obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(X, Y);
        }

        public static bool operator ==(Location left, Location right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Location left, Location right)
        {
            return !Equals(left, right);
        }

        #endregion
    }
}