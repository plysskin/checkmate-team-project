﻿using System;

namespace ChessOnline.MessageProtocol.Requests
{
    public record StartGameRequest
    {
        public Guid FirstPlayer { get; init; }
        public Guid SecondPlayer { get; init; }
    }
}
