﻿namespace ChessOnline.MessageProtocol.Requests
{
    public record UserInfoRequest
    {
        public System.Guid Id { get; init; }
    }

}