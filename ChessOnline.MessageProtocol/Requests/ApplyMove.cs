﻿using System;

namespace ChessOnline.MessageProtocol.Requests
{
    public record ApplyMoveRequest
    {
        public Guid GameId { get; init; }
        public Guid FigureId { get; init; }
        public int NewLocationX { get; init; }
        public int NewLocationY { get; init; }
    }
}
