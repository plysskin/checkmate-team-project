﻿namespace ChessOnline.MessageProtocol
{
    public interface IProcessor
    {
        Response<U> OnRequest<T, U>(Request<T> request);
    }
}