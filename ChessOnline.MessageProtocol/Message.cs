using ChessOnline.MessageProtocol.Responses;

namespace ChessOnline.MessageProtocol
{
    public enum MessageType 
    {
        NewUser,
        GetRival,
        ApplyMove,
        StartGame
    }

    public record Request<T>(T Body, MessageType MessageType);

    public record Response<T>(bool Success, T Body, ErrorResponse Error);
}