using System;
using ChessOnline.Extensions;

namespace ChessOnline.MessageProtocol.Responses
{
    public record ErrorResponse
    {
        private readonly ErrorType _type;
        private readonly Exception _ex;
        
        public string Message { get; }
        public string Type { get; }

        private ErrorResponse(ErrorType type, Exception ex)
        {
            _type = type;
            Type = type.ToString();
            Message = ex.Message;
            _ex = ex;
        }
        
        public static ErrorResponse ExceptionToError(Exception ex)
        {
            return ex.GetType() == typeof(ChessOnlineException) 
                ? new ErrorResponse(((ChessOnlineException) ex).Type, ex) 
                : new ErrorResponse(ErrorType.ERR_UNEXPECTED, ex);
        }

        public static bool TryExtract<T>(Response<T> response, out ErrorResponse error)
        {
            if (response == null || response.Success && response.Body == null ||
                !response.Success && response.Error == null)
            {
                var ex = new ChessOnlineException("Unexpected value: The response was null."
                    , ErrorType.ERR_UNEXPECTED);
                
                error = new ErrorResponse(ex.Type, ex);
                return true;
            }

            if (response.Error != null)
            {
                error = response.Error;
                return true;
            }

            error = null;
            return false;
        }

        public ErrorType GetErrorType()
        {
            return _type;
        }

        public Exception GetException()
        {
            return _ex;
        }
    }
}