﻿namespace ChessOnline.MessageProtocol.Responses
{
    public class AuthResponse
    {
        public System.Guid Id { get; set; }
    }
}