using System;

namespace ChessOnline.MessageProtocol.Responses
{
    public record ApplyMoveResponse
    {
        public Guid FigureId { get; init; }
        public int NewPositionX { get; init; }
        public int NewPositionY { get; init; }
        public bool IsValid { get; init; }
    }
}