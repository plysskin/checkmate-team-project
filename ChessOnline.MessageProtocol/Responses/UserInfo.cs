﻿namespace ChessOnline.MessageProtocol.Responses
{
    public record UserInfoResponse
    {
        public System.Guid Id { get; init; }
        public string Name { get; init; }
    }
}
