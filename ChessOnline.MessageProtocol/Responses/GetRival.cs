namespace ChessOnline.MessageProtocol.Responses
{
    public record GetRivalResponse : UserInfoResponse
    {
        public string Status { get; init; }
    }
}