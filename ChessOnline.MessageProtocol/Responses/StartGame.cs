using System;

namespace ChessOnline.MessageProtocol.Responses
{
    public record StartGameResponse
    {
        public string Status { get; init; }
        public Guid GameId { get; init; }
        public PlayerInfo[] Players { get; init; }
        public FigureInfo[] Figures { get; init; }
    }

    public record FigureInfo
    {
        public Guid Id { get; init; }
        public string Color { get; init; }
        public string Type { get; init; }
        public int LocationX { get; init; }
        public int LocationY { get; init; }
    }

    public record PlayerInfo
    {
        public Guid Id { get; set; }
        public string Color { get; set; }
    }
}