using System;

namespace ChessOnline.Extensions
{
    public enum ErrorType
    {
        ERR_API,
        ERR_DATA,
        ERR_SERVER,
        ERR_STATUS,
        ERR_UNEXPECTED,
    }
    
    public class ChessOnlineException : Exception
    {
        public ChessOnlineException(string message, ErrorType type) : base(message)
        {
            Type = type;
        }

        public ErrorType Type { get; }
    }
}