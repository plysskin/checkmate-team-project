namespace ChessOnline.Extensions
{
    public enum StartGameStatus
    {
        Started,
        AlreadyStarted
    }
}