﻿using System;
using ChessOnline.Data.Interfaces;

namespace Matchmaking
{
    public class GetUser
    {
        /// <summary>
        /// Возвращает пользователя из коллекции
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public IUser GetUserById(Guid userId)
        {
            var userFromCollection = UsersCollection.GetUser(userId);
            if (userFromCollection is null) {
                throw new NullReferenceException("Пользователь с указанным идентификатором не найден в системе.");
            }
            return userFromCollection;
        }

        /// <summary>
        /// Возвращает пару для пользователя
        /// </summary>
        /// <returns></returns>
        public bool TryGetReadyUser(Guid requestedUserId, out IUser readyUser)
        {
            readyUser = UsersCollection.GetReadyUser(requestedUserId);
            return readyUser != null;
        }

        public IUser GetLinkedUser(Guid userId)
        {
            return UsersCollection.GetLinkedUser(userId);
        }
    }
}
