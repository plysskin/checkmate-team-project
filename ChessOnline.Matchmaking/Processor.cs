﻿using System;
using ChessOnline.Extensions;
using ChessOnline.MessageProtocol;
using ChessOnline.MessageProtocol.Requests;
using ChessOnline.MessageProtocol.Responses;

namespace Matchmaking
{
    public class MatchmakingProcessor : IProcessor
    {
        private readonly UserService _userService;
        
        public MatchmakingProcessor()
        {
            _userService = new UserService();
        }
        
        public Response<U> OnRequest<T, U>(Request<T> request)
        {
            var responseBody = request.MessageType switch
            {
                //Получение идентификатора и имени нового игрока, отправка на клиента
                MessageType.NewUser => (U)Convert.ChangeType(GetNewUser(), typeof(U)),
                //Получение идентификатора и имени соперника, отправка на клиента
                MessageType.GetRival => (U)Convert.ChangeType(GetRival((request.Body as UserInfoRequest)), typeof(U)),
                _ => throw new ChessOnlineException($"Unrecognized message type: {request.MessageType}.",
                    ErrorType.ERR_UNEXPECTED)
            };

            return new Response<U>(true, responseBody, null);
        }

        /// <summary>
        /// Создаёт нового пользователя
        /// </summary>
        /// <returns></returns>
        private UserInfoResponse GetNewUser()
        {
            var user = _userService.GetNewUser();
            var response = new UserInfoResponse 
            {
                Id = user.Id,
                Name = user.Name
            };

            return response;
        }

        private GetRivalResponse GetRival(UserInfoRequest request)
        {
            var status = _userService.GetRival(request.Id, out var rival);

            return new GetRivalResponse
            {
                Id = rival?.Id ?? default,
                Name = rival?.Name,
                Status = status.ToString()
            };
        }
    }
}