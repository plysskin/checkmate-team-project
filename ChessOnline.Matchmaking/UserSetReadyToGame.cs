﻿using System;
using ChessOnline.Data.Enums;

namespace Matchmaking
{
    public class UserSetReadyToGame
    {
        
        /// <summary>
        /// Выставляет Состояние пользователю о том что он готов к игре
        /// </summary>
        /// <param name="user"></param>
        public void SetReadyToGame(Guid userId)
        {
            SetUserState(userId, StateUserEnum.IsReadyToGame);
        }

        public void SetInGame(Guid firstUserId, Guid secondUserId)
        {
            SetUserState(firstUserId, StateUserEnum.InGame);
            SetUserState(secondUserId, StateUserEnum.InGame);
            UsersCollection.LinkUsers(firstUserId, secondUserId);
        }

        /// <summary>
        /// Выставляет указанное состояние указанному пользователю
        /// </summary>
        /// <param name="user"></param>
        /// <param name="stateUserEnum"></param>
        private void SetUserState(Guid userId, StateUserEnum stateUserEnum)
        {
            var userFromCollection = new GetUser().GetUserById(userId);
            userFromCollection.State = stateUserEnum;
        }
    }
}
