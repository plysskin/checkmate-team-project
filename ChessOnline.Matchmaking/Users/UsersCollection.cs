﻿using ChessOnline.Data.Enums;
using ChessOnline.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using ChessOnline.Extensions;

namespace Matchmaking
{
    internal static class UsersCollection
    {
        private static List<IUser> _allUsers = new List<IUser>();
        private static Dictionary<Guid, Guid> _linkedUserIds = new(); // Пользователи, которые свзязались между собой
        /// <summary>
        /// Временный список всех пользователей на сайте        
        /// </summary>
        private static List<IUser> AllUsers
        {
            get => _allUsers;
            set { _allUsers = value; }
        }

        /// <summary>
        /// Добавляет пользователя в коллекцию
        /// </summary>
        /// <param name="user"></param>
        public static void AddUser(IUser user)
        {
            AllUsers.Add(user);
        }

        /// <summary>
        /// Возвращает готового к игре пользователя
        /// </summary>
        /// <returns></returns>
        public static IUser GetReadyUser(Guid requestedUserId)
        {
            return AllUsers.FirstOrDefault(x => x.State == StateUserEnum.IsReadyToGame && x.Id != requestedUserId);
        }

        /// <summary>
        /// Возврадщает пользователя по идентификатору
        /// </summary>
        /// <param name="userGuid">GUID пользователя</param>
        /// <returns></returns>
        public static IUser GetUser(Guid userGuid)
        {
            return AllUsers.FirstOrDefault(x => x.Id == userGuid);
        }

        public static void LinkUsers(Guid firstUserId, Guid secondUserId)
        {
            if (GetUser(firstUserId) == null || GetUser(secondUserId) == null)
            {
                throw new ChessOnlineException($"Не удалось связать пользователя {firstUserId} c пользователем" +
                                               $" {secondUserId}. Пользователи отсутствуют в коллекции."
                    , ErrorType.ERR_SERVER);
            }

            _linkedUserIds[firstUserId] = secondUserId;
            _linkedUserIds[secondUserId] = firstUserId;
        }

        public static IUser GetLinkedUser(Guid userId)
        {
            var linkedUserId = _linkedUserIds[userId];
            return GetUser(linkedUserId);
        }
    }
}
