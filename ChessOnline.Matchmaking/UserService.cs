using System;
using ChessOnline.Data.Enums;
using ChessOnline.Data.Interfaces;
using ChessOnline.Extensions;

namespace Matchmaking
{
    public class UserService
    {
        public IUser GetNewUser()
        {
            var newUserGameInfo = new InitilizeUser().CreateNewUser();
            UsersCollection.AddUser(newUserGameInfo);
            return newUserGameInfo;
        }

        public GetRivalStatus GetRival(Guid userId, out IUser rival)
        {
            var getUser = new GetUser();
            var user = getUser.GetUserById(userId);
            rival = null;
            
            switch (user.State)
            {
                case StateUserEnum.NotReadyGame:
                    new UserSetReadyToGame().SetReadyToGame(userId);
                    return GetRivalStatus.Placed;
                case StateUserEnum.IsReadyToGame:
                    if (!getUser.TryGetReadyUser(userId, out rival))
                    {
                        return GetRivalStatus.InProgress;
                    }
                    
                    var userSetReadyToGame = new UserSetReadyToGame();
                    userSetReadyToGame.SetInGame(userId, rival.Id);
                    return GetRivalStatus.Done;
                case StateUserEnum.InGame:
                    rival = getUser.GetLinkedUser(userId);
                    return GetRivalStatus.Done;
                default:
                    throw new ChessOnlineException($"Неожидаемое состояние пользователя: {user.State}."
                        , ErrorType.ERR_UNEXPECTED);
            }
        }
    }
}