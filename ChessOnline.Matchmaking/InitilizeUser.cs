﻿using ChessOnline.Data.Interfaces;
using System;

namespace Matchmaking
{
    public class InitilizeUser
    {
        private Random _random = new Random();
        public InitilizeUser()
        {

        }

        /// <summary>
        /// Создаёт нового пользователя
        /// </summary>
        /// <returns></returns>
        public IUser CreateNewUser()
        {
            return new ChessOnline.Data.Models.UserGameInfo
            {
                Id = CreateNewUserId(),
                Name = CreateRandomNewNameForUser(),
                State = ChessOnline.Data.Enums.StateUserEnum.NotReadyGame
            };
        }


        private Guid CreateNewUserId()
        {
            return Guid.NewGuid();
        }

        private string CreateRandomNewNameForUser()
        {
            string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZqwertyuiopasdfghjklzxcvbnm1234567890";
            var userName = "";
            while (userName.Length < 10) {
                userName += chars[_random.Next(0, chars.Length)];
            }
            return "user_" + userName;
        }
    }
}
