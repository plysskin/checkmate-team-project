﻿using System;
using ChessOnline.MessageProtocol;

namespace ChessOnline.API.Enums
{
    public enum ComponentType
    {
        Matchmaking,
        Game,
        GameV2
    }

    internal static class ComponentExtensions
    {
        public static ComponentType GetComponentType(this MessageType messageType)
        {
            switch (messageType)
            {
                case MessageType.NewUser:
                case MessageType.GetRival:
                    return ComponentType.Matchmaking;
                case MessageType.ApplyMove:
                case MessageType.StartGame:
                    return ComponentType.GameV2;
                default:
                    throw new ArgumentException($"Unrecognized message type: {messageType}.");
            }
        }
    }
}