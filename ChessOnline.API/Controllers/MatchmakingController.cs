﻿using ChessOnline.API.Hubs;
using ChessOnline.MessageProtocol;
using ChessOnline.MessageProtocol.Requests;
using ChessOnline.MessageProtocol.Responses;
using Microsoft.AspNetCore.Mvc;

namespace ChessOnline.API.Controllers
{
    [Route("api/matchmaking")]
    public class MatchmakingController : BaseController
    {
        private readonly ConnectionMapper _connections;

        public MatchmakingController(Bridge bridge, ConnectionMapper connections) : base(bridge)
        {
            _connections = connections;
        }

        [Route("getNewUser")]
        [HttpGet]
        public IActionResult GetNewUser()
        {
            return OnRequest<object, UserInfoResponse>(null, MessageType.NewUser);
        }

        [Route("getRival")]
        [HttpPost]
        public IActionResult GetRival([FromBody] UserInfoRequest body)
        {
            return OnRequest<UserInfoRequest, GetRivalResponse>(body, MessageType.GetRival);
        }
    }
}