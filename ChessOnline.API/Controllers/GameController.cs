using ChessOnline.API.Hubs;
using ChessOnline.Extensions;
using ChessOnline.MessageProtocol;
using ChessOnline.MessageProtocol.Requests;
using ChessOnline.MessageProtocol.Responses;
using Microsoft.AspNetCore.Mvc;

namespace ChessOnline.API.Controllers
{
    [Route("api/game")]
    public class GameController : BaseController
    {
        private readonly ConnectionMapper _connections;
        
        public GameController(Bridge bridge, ConnectionMapper connections) : base(bridge)
        {
            _connections = connections;
        }

        [Route("start")]
        [HttpPost]
        public IActionResult StartGame([FromBody] StartGameRequest body)
        {
            var result = OnRequest<StartGameRequest, StartGameResponse>(body, MessageType.StartGame, out var response);

            if (!ErrorResponse.TryExtract(response, out _) &&
                response.Body.Status == StartGameStatus.Started.ToString())
            {
                MapUsers(body.FirstPlayer.ToString(), body.SecondPlayer.ToString());
            }

            return result;
        }
        
        private void MapUsers(string firstUser, string secondUser)
        {
            if (string.IsNullOrEmpty(firstUser) || string.IsNullOrEmpty(secondUser))
            {
                return;
            }

            try
            {
                _connections.Map(firstUser, secondUser);
            }
            catch (ChessOnlineException)
            {
            }
        }
    }
}