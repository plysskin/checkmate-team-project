using Microsoft.AspNetCore.Mvc;
using ChessOnline.Extensions;
using ChessOnline.API.ActionResults;
using ChessOnline.MessageProtocol;
using ChessOnline.MessageProtocol.Responses;

namespace ChessOnline.API.Controllers
{
    public abstract class BaseController : ControllerBase
    {
        private readonly Bridge _bridge;
        
        protected BaseController(Bridge bridge)
        {
            _bridge = bridge;
        }
        
        protected IActionResult OnRequest<T, U>(T body, MessageType type, out Response<U> response)
        {
            response = _bridge.OnRequest<T, U>(new Request<T>(body, type));
            return ResponseToActionResult(response);
        }
        
        protected IActionResult OnRequest<T, U>(T body, MessageType type)
        {
            return OnRequest<T, U>(body, type, out _);
        }

        private IActionResult ResponseToActionResult<T>(Response<T> response)
        {
            if (!ErrorResponse.TryExtract(response, out var error))
            {
                return Ok(response.Body);
            }

            return error.GetErrorType() switch
            {
                ErrorType.ERR_DATA or ErrorType.ERR_STATUS => BadRequest(response.Error),
                _ => InternalError(response.Error)
            };
        }

        private static InternalServerErrorObjectResult InternalError(object value)
        {
            return new(value);
        }
    }
}