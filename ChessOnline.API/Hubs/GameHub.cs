﻿using System.Threading.Tasks;
using ChessOnline.Extensions;
using ChessOnline.MessageProtocol;
using ChessOnline.MessageProtocol.Requests;
using ChessOnline.MessageProtocol.Responses;
using Microsoft.AspNetCore.SignalR;

namespace ChessOnline.API.Hubs
{
    internal class GameHub : Hub
    {
        private readonly Bridge _bridge;
        private readonly ConnectionMapper _connections;

        public GameHub(Bridge bridge, ConnectionMapper connections)
        {
            _bridge = bridge;
            _connections = connections;
        }
        
        public void StandToQueue(string userId)
        {
            CheckUserId(userId);
            _connections.Add(userId, Context.ConnectionId);
        }

        public async Task ApplyMove(ApplyMoveRequest body, string userId)
        {
            try
            {
                //Получаем SignalR ConnectionId игроков
                CheckUserId(userId);
                var userConnectionId = _connections.Get(userId);
                var rivalConnectionId = _connections.GetMappedConnection(userId);

                //Делаем ход
                var request = new Request<ApplyMoveRequest>(body, MessageType.ApplyMove);
                // var response = ApplyMove(request);
                var response = _bridge.OnRequest<ApplyMoveRequest, ApplyMoveResponse>(request);

                //Отправляем результат первому и второму игроку
                if (ErrorResponse.TryExtract(response, out var error))
                {
                    await Clients.Clients(new[] {userConnectionId, rivalConnectionId}).SendAsync(
                        "onAppliedMove", error);
                }
                else
                {
                    await Clients.Clients(new[] {userConnectionId, rivalConnectionId}).SendAsync(
                        "onAppliedMove", response.Body);
                }
            }
            catch (ChessOnlineException ex)
            {
                throw new HubException("Не удается применить ход.", ex);
            }
        }

        //Заглушка для валидации хода короля
        // private Response<ApplyMoveResponse> ApplyMove(Request<ApplyMoveRequest> request)
        // {
        //     var reqBody = request.Body;
        //     var respBody = new ApplyMoveResponse
        //     {
        //         PieceID = reqBody.PieceID,
        //         NewPositionX = reqBody.NewPositionX,
        //         NewPositionY = reqBody.NewPositionY,
        //         IsValid = true
        //     };
        //     
        //     return new(true, respBody, null);
        // }

        private void CheckUserId(string userId)
        {
            if (string.IsNullOrEmpty(userId))
            {
                throw new HubException("Поле Id пользователя пустое.");
            }
        }
    }
}