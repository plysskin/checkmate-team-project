﻿using System.Collections.Concurrent;
using ChessOnline.Extensions;

namespace ChessOnline.API.Hubs
{
    public class ConnectionMapper
    {
        // Словарь подключений, где ключ - наш Id игрока, значение - SignalR ConnectionId
        private readonly ConcurrentDictionary<string, string> _connections;
        // Связанные подключения, где оба значения - наш Id игроков, которые играют между собой
        private readonly ConcurrentDictionary<string, string> _mappedUsers;

        public ConnectionMapper()
        {
            _connections = new ConcurrentDictionary<string, string>();
            _mappedUsers = new ConcurrentDictionary<string, string>();
        }

        public void Add(string user, string connection)
        {
            if (!_connections.TryAdd(user, connection))
            {
                throw new ChessOnlineException($"Failed to add User {user} connection id {connection}."
                    , ErrorType.ERR_API);
            }
        }

        public string Get(string user)
        {
            if (!_connections.TryGetValue(user, out var connection))
            {
                throw new ChessOnlineException($"Failed to get User {user} connection id {connection}."
                    , ErrorType.ERR_API);
            }

            return connection;
        }

        public string GetMappedConnection(string user)
        {
            if (!_mappedUsers.TryGetValue(user, out var mappedUser)
                || !_connections.TryGetValue(mappedUser, out var mappedConnection))
            {
                throw new ChessOnlineException($"Failed to get mapped connection id for User {user}."
                    , ErrorType.ERR_API);
            }

            return mappedConnection;
        }

        public void Map(string firstUser, string secondUser)
        {
            if (!_connections.ContainsKey(firstUser) || !_connections.ContainsKey(secondUser))
            {
                throw new ChessOnlineException($"Failed to map User {firstUser} with User {secondUser}." +
                    " User does not exist.", ErrorType.ERR_API);
            }
            
            //Добавляем по два значения для успешного поиска в GetMappedConnection
            if (!_mappedUsers.TryAdd(firstUser, secondUser)
                || !_mappedUsers.TryAdd(secondUser, firstUser))
            {
                throw new ChessOnlineException($"Failed to map User {firstUser} with User {secondUser}."
                    , ErrorType.ERR_API);
            }
        }
    }
}