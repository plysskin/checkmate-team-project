﻿using System;
using System.Collections.Generic;
using ChessOnline.API.Enums;
using ChessOnline.GameV2;
using ChessOnline.MessageProtocol;
using Matchmaking;

namespace ChessOnline.API
{
    public class ProcessorFactory
    {
        private readonly IDictionary<ComponentType, IProcessor> _processors;

        public ProcessorFactory()
        {
            _processors = new Dictionary<ComponentType, IProcessor>();
        }
        
        public IProcessor GetProcessor(ComponentType component)
        {
            lock (_processors)
            {
                if (!_processors.TryGetValue(component, out var processor))
                {
                    processor = CreateProcessor(component);
                    _processors.Add(component, processor);
                }

                return processor;
            }
        }

        private static IProcessor CreateProcessor(ComponentType component)
        {
            return component switch
            {
                ComponentType.Matchmaking => new MatchmakingProcessor(),
                ComponentType.Game => new GameProcessor(),
                ComponentType.GameV2 => new GameV2Processor(),
                _ => throw new ArgumentOutOfRangeException(nameof(component), component, null)
            };
        }
    }
}