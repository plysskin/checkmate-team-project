﻿using System;
using ChessOnline.API.Enums;
using ChessOnline.MessageProtocol;
using ChessOnline.MessageProtocol.Responses;

namespace ChessOnline.API
{
    public class Bridge
    {
        private readonly ProcessorFactory _processorFactory;
        
        public Bridge(ProcessorFactory processorFactory)
        {
            _processorFactory = processorFactory;
        }
        
        public Response<U> OnRequest<T, U>(Request<T> request)
        {
            try
            {
                var component = request.MessageType.GetComponentType();
                var processor = _processorFactory.GetProcessor(component);

                return processor.OnRequest<T, U>(request);
            }
            catch (Exception ex)
            {
                return new Response<U>(false, default, ErrorResponse.ExceptionToError(ex));
            }
        }
    }
}