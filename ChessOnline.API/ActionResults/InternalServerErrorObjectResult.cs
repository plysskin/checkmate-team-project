using Microsoft.AspNetCore.Mvc;

namespace ChessOnline.API.ActionResults
{
    internal class InternalServerErrorObjectResult : ObjectResult
    {
        public InternalServerErrorObjectResult(object value) : base(value)
        {
            StatusCode = 500;
        }
    }
}