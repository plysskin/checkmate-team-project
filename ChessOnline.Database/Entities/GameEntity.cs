﻿using System;
using System.Collections.Generic;
using System.Text;
using NHibernate.Mapping.Attributes;

namespace DBProject.Entities
{
    [Class(Table = "games")]
    public class GameEntity
    {
        [Id(0, Name = "Id")]
        [Generator(1, Class = "native")]
        public virtual long Id { get; set; }

        [ManyToOne(Column = "user1_id", ForeignKey = "game_fk_user1_id", Cascade = "all")]
        public virtual UserEntity User1 { get; set; }

        [ManyToOne(Column = "user2_id", ForeignKey = "game_fk_user2_id", Cascade = "all")]
        public virtual UserEntity User2 { get; set; }

        [Property(NotNull = true)]
        public virtual GameResult Result { get; set; }

        [Property(NotNull = true)]
        public virtual DateTime Time { get; set; }

        //[Bag(0, Name = "Moves", Inverse = true)]
        //[Key(1, Column = "GameId")]
        //[OneToOne(2, ClassType = typeof(MoveEntity))]
        //public virtual IList<MoveEntity> Moves { get; set; }
    }

    public enum GameResult
    {
        win_white,
        win_black,
        draw
    }
}
