﻿using System;
using System.Collections.Generic;
using System.Text;
using NHibernate.Mapping.Attributes;

namespace DBProject.Entities
{
    [Class(Table = "users")]
    public class UserEntity
    {        
        [Id(0, Name = "Id")]
        [Generator(1, Class = "native")]
        public virtual long Id { get; set; }

        [Property(NotNull = true)]
        public virtual string FirstName { get; set; }

        [Property]
        public virtual string LastName { get; set; }

        [Property]
        public virtual string MiddleName { get; set; }

        [Property(NotNull = true, Unique = true, UniqueKey = "users_email_unique")]
        public virtual string Email { get; set; }

        [Property(NotNull = true, Unique = true, UniqueKey = "users_username_unique")]
        public virtual string UserName { get; set; }

        [Property(NotNull = true, Unique = true)]
        public virtual string Password { get; set; }
    }

}
