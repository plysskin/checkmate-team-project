﻿using System;
using System.Collections.Generic;
using System.Text;
using NHibernate.Mapping.Attributes;

namespace DBProject.Entities
{
    [Class(Table = "moves")]
    public class MoveEntity
    {
        [Id(0, Name = "Id")]
        [Generator(1, Class = "native")]
        public virtual long Id { get; set; }        

        [Property(NotNull = true)]
        public virtual TypeFigure TypeFigure { get; set; }

        [Property(NotNull = true)]
        public virtual Side Side { get; set; }

        [Property(NotNull = true)]
        public virtual string StartPosition { get; set; }

        [Property(NotNull = true)]
        public virtual string EndPosition { get; set; }

        [Property(0, NotNull = true)]
        [ManyToOne(1, ClassType = typeof(GameEntity))]
        public virtual GameEntity GameId { get; set; }
    }

    public enum TypeFigure
    {
        pawn,
        knight,
        bishop,
        rook,
        queen,
        king
    }
    public enum Side
    {
        black,
        white
    }
    
    public class Location
    {
        public Letter Letter { get; set; }
        public int Number { get; set; }
        public Location(string loc)
        {
            Letter = (Letter)Enum.Parse(typeof(Letter), loc[0].ToString());
            Number = Convert.ToInt32(loc[1]);
        }

        public string getString()
        {
            return Letter + Number.ToString();
        }
    }
    public enum Letter
    {
        A,
        B,
        C,
        D,
        E,
        F,
        G,
        H
    }

}
