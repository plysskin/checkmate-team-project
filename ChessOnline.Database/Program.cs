﻿using DBProject.Entities;
using DBProject.Storage;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Mapping.Attributes;
using NHibernate.Tool.hbm2ddl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace DBProject
{
    class Program
    {
        public static string connectionString { get; set; }
        static ISessionFactory sessionFactory;
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }
        static void CreateOrUpdateUser(long Id, string FirstName, string LastName, string MiddleName, string Email, string UserName, string Password)
        {
            var storage = new UsersStorage(SessionFactory);
            var client = new UserEntity
            {
                Id = Id,
                FirstName = FirstName,
                LastName = LastName,
                MiddleName = MiddleName,
                Email = Email,
                UserName = UserName,
                Password = Password
            };
            storage.SaveOrUpdate(client);
        }
        static void CreateOrUpdateGame(long Id, long User1Id, long User2Id, string Result, DateTime Time)
        {
            var storage = new GamesStorage(SessionFactory);
            var storageUsers = new UsersStorage(SessionFactory);
            var User1 = storageUsers.FindUserById(User1Id);
            var User2 = storageUsers.FindUserById(User2Id);

            var client = new GameEntity
            {
                Id = Id,
                User1 = User1,
                User2 = User2,
                Result = (GameResult)Enum.Parse(typeof(GameResult), Result),
                Time = Time
            };
            storage.SaveOrUpdate(client);
        }
        static void CreateOrUpdateMove(long Id, long GameId, string TypeFigure, string Side, string StartPosition, string EndPosition)
        {
            var storage = new MovesStorage(SessionFactory);
            var storageGame = new GamesStorage(SessionFactory);
            var Game = storageGame.FindGameById(GameId);
            var client = new MoveEntity
            {
                Id = Id,
                TypeFigure = (TypeFigure)Enum.Parse(typeof(TypeFigure), TypeFigure),
                Side = (Side)Enum.Parse(typeof(Side), Side),
                StartPosition = StartPosition,
                EndPosition = EndPosition,
                GameId = Game
            };
            storage.SaveOrUpdate(client);
        }
        static void GetHistoryGame(long GameId)
        {
            using (ISession session = SessionFactory.OpenSession())
            {
                using (ITransaction tx = session.BeginTransaction())
                {
                    var result = session.Query<GameEntity>()
                        .Where(it => it.Id == GameId)
                        .ToList();
                    if (result.Count > 0)
                    {
                        var Game = result[0];
                        var User1 = Game.User1;
                        var User2 = Game.User2;
                        var moves = session.Query<MoveEntity>()
                        .Where(it => it.Id == GameId)
                        .ToList();
                        var res = Resource1.ResourceManager;
                        Console.WriteLine("Игрок 1 - " + User1.UserName + ", Игрок 2 - " + User2.UserName + "\nРезультат игры -  " + res.GetString(Game.Result.ToString()) + "\nВремя игры - " + Game.Time);

                        int i = 0;
                        foreach(var move in moves)
                        {
                            i++;
                            Console.WriteLine("Ход " + i + ":" + res.GetString(move.TypeFigure.ToString()) + ", смена позиции с " + move.StartPosition + " на " + move.EndPosition);
                        }
                    }
                    else throw new Exception("Игра не найдена");
                    
                }
            }
        }

        static ISessionFactory SessionFactory
        {
            get
            {
                if (sessionFactory == null)
                {
                    var configProperties = new Dictionary<string, string>
                    {
                        { NHibernate.Cfg.Environment.ConnectionDriver, typeof (NHibernate.Driver.NpgsqlDriver).FullName },
                        { NHibernate.Cfg.Environment.Dialect, typeof (NHibernate.Dialect.PostgreSQL82Dialect).FullName },
                        { NHibernate.Cfg.Environment.ConnectionString, connectionString },
                        //{ "show_sql", "true" }
                    };

                    var serializer = HbmSerializer.Default;
                    serializer.Validate = true;

                    var configuration = new Configuration()
                        .SetProperties(configProperties)
                        //.SetNamingStrategy(new PostgresNamingStrategy())
                        .AddInputStream(serializer.Serialize(Assembly.GetExecutingAssembly()));
                        //.SetInterceptor(new SqlDebugOutputInterceptor());

                    new SchemaUpdate(configuration).Execute(true, true);

                    sessionFactory = configuration.BuildSessionFactory();
                }
                return sessionFactory;
            }
        }
    }
}
