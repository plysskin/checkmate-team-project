﻿using DBProject.Entities;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DBProject.Storage
{
    class GamesStorage
    {
        private readonly ISessionFactory sessionFactory;

        public GamesStorage(ISessionFactory sessionFactory)
        {
            this.sessionFactory = sessionFactory;
        }
        public void SaveOrUpdate(GameEntity entity)
        {
            using (ISession session = sessionFactory.OpenSession())
            {
                using (ITransaction tx = session.BeginTransaction())
                {
                    session.SaveOrUpdate(entity);
                    tx.Commit();
                }
            }
        }
        public GameEntity FindGameById(long GameId)
        {
            using (ISession session = sessionFactory.OpenSession())
            {
                using (ITransaction tx = session.BeginTransaction())
                {
                    return session.Query<GameEntity>()
                        .Where(it => it.Id == GameId)
                        .ToList()[0];
                }
            }
        }
    }
}
