﻿using DBProject.Entities;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DBProject.Storage
{
    class UsersStorage
    {
        private readonly ISessionFactory sessionFactory;

        public UsersStorage(ISessionFactory sessionFactory)
        {
            this.sessionFactory = sessionFactory;
        }
        public void SaveOrUpdate(UserEntity entity)
        {
            using (ISession session = sessionFactory.OpenSession())
            {
                using (ITransaction tx = session.BeginTransaction())
                {
                    session.SaveOrUpdate(entity);
                    tx.Commit();
                }
            }
        }
        public UserEntity FindUserById(long UserId)
        {
            using (ISession session = sessionFactory.OpenSession())
            {
                using (ITransaction tx = session.BeginTransaction())
                {
                    return session.Query<UserEntity>()
                        .Where(it => it.Id == UserId)
                        .ToList()[0];
                }
            }
        }
    }
}
