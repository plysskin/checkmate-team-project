﻿using DBProject.Entities;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DBProject.Storage
{
    class MovesStorage
    {
        private readonly ISessionFactory sessionFactory;

        public MovesStorage(ISessionFactory sessionFactory)
        {
            this.sessionFactory = sessionFactory;
        }
        public void SaveOrUpdate(MoveEntity entity)
        {
            using (ISession session = sessionFactory.OpenSession())
            {
                using (ITransaction tx = session.BeginTransaction())
                {
                    session.SaveOrUpdate(entity);
                    tx.Commit();
                }
            }
        }
    }
}
