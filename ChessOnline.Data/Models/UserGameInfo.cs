﻿using ChessOnline.Data.Enums;
using ChessOnline.Data.Interfaces;
using System;

namespace ChessOnline.Data.Models
{
    public class UserGameInfo : IUser
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public StateUserEnum State { get; set; }
        public IGame Game { get; set; }
    }
}
