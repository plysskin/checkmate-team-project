﻿namespace ChessOnline.Data.Interfaces
{
    public interface IUser
    {
        /// <summary>
        /// Айди пользователя
        /// </summary>
        System.Guid Id { get; set; }

        /// <summary>
        /// Наименование пользователдя
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// Состояние игрока
        /// </summary>
        Enums.StateUserEnum State { get; set; }

        /// <summary>
        /// Игра пользователя текущая
        /// </summary>
        IGame Game {get;set; }
    }
}
