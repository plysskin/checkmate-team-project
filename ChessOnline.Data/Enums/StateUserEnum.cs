﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChessOnline.Data.Enums
{
    public enum StateUserEnum
    {
        Unknown = 0,
        /// <summary>
        /// Не готов
        /// </summary>
        NotReadyGame = 1,

        /// <summary>
        /// Готов к игре
        /// </summary>
        IsReadyToGame = 2,

        /// <summary>
        /// В игре
        /// </summary>
        InGame = 3
    }
}
