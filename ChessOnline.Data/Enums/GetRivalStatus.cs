namespace ChessOnline.Data.Enums
{
    public enum GetRivalStatus
    {
        Placed,
        InProgress,
        Done
    }
}